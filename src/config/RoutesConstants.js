export const INDEX_ROUTE = `/`;
export const ITEMS_SEARCH = `/items`;
export const ITEM = `/items/:id`;
