import React from "react";
import { Switch, Route } from "react-router-dom";
import home from "../screens/home/home";
import search from "../screens/search/search";
import item from "../screens/item/item";
import {
	INDEX_ROUTE,
	ITEMS_SEARCH,
	ITEM
} from "./RoutesConstants";

const Routes = () => {
	return (
		<Switch>
			<Route path={ITEM} component={item} />
			<Route path={ITEMS_SEARCH} component={search} />
			<Route path={INDEX_ROUTE} component={home} />
		</Switch>
	);
};

export default Routes;
