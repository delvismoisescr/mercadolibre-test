export default class Item {

    static getItem(id, filters, callback) {
        return new Promise(async () => {
            try {
                let response = await fetch(`http://localhost:3005/api/items/${id}`, {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        name: filters.name,
                        lastname: filters.lastname
                    }),
                });
                let responseJson = await response.json();
                callback(responseJson.data);
            } catch (error) {
                callback(error);
            }
        });
    }

};
