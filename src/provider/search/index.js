export default class Search {

    static searchItems(value, filters, callback) {
        return new Promise(async () => {
            try {
                let response = await fetch(
                    `http://localhost:3005/api/items?q=${value}&limit=${filters.limit}&name=${filters.name}&lastname=${filters.lastname}`
                );
                let responseJson = await response.json();
                callback(responseJson.data);
            } catch (error) {
                callback(error);
            }
        });
    }

};
