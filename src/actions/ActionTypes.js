// SEARCH
export const SEARCH_ITEMS = "SEARCH_ITEMS";
export const UPDATE_SEARCH = "UPDATE_SEARCH";
export const RESET_SEARCH = "RESET_SEARCH";

// ITEM
export const GET_ITEM = "GET_ITEM";
export const RESET_ITEM = "RESET_ITEM";