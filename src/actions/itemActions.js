import { GET_ITEM, RESET_ITEM } from "./ActionTypes";
import Item from '../provider/item';

// RESETEA EL REDUCER DE ITEM
export function resetItem() {
    return dispatch => {
        dispatch({ type: RESET_ITEM });
    };
}

// REALIZA LA PETICIÓN A LA API PARA OBTENER UN ITEM
export function getItem(id, filters) {
    return dispatch => {
        asyncGetItem(id, filters, (response) => {
            // SI LA RESPUESTA ES CORRECTA HAGO CAMBIOS EN EL REDUCER DE ITEM
            if (response) {
                dispatch({ type: GET_ITEM, ...response });
            }
        });
    };
}

// REALIZA LA PETICIÓN A LA API PARA OBTENER UN ITEM
async function asyncGetItem(id, filters, callback) {
    try {
        await Item.getItem(id, filters, (response) => {
            callback(response)
        });
    } catch (error) {
        callback(false);
    }
}