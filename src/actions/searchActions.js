import { SEARCH_ITEMS, UPDATE_SEARCH, RESET_SEARCH } from "./ActionTypes";
import Search from '../provider/search';

// RESETEA EL REDUCER DE BUSQUEDA
export function resetProps() {
    return dispatch => {
        dispatch({ type: RESET_SEARCH });
    };
}

// ACTUALIZA EL VALOR DE LA CAJA DE BUSQUEDA
export function updateSearch(value) {
    return dispatch => {
        dispatch({ type: UPDATE_SEARCH, search: value });
    };
}

// REALIZA LA PETICIÓN A LA API PARA OBTENER UN LISTADO DE ITEMS
export function searchItems(value, filters) {
    return dispatch => {
        asyncSearchItems(value, filters, (response) => {
            if (response) {
                dispatch({ type: SEARCH_ITEMS, props: response, loading: false, search: value });
            }
        });
    };
}

// REALIZA LA PETICIÓN A LA API PARA OBTENER UN LISTADO DE ITEMS
async function asyncSearchItems(value, filters, callback) {
    try {
        await Search.searchItems(value, filters, (response) => {
            callback(response)
        });
    } catch (error) {
        callback(false);
    }
}