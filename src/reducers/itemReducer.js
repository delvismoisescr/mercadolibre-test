import {
    GET_ITEM,
    RESET_ITEM
} from '../actions/ActionTypes';

const INITIAL_STATE = {
    author: {},
    item: {},
    categories: [],
    loading: true
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case RESET_ITEM:
            return { ...INITIAL_STATE };

        case GET_ITEM:
            return { ...state, author: action.author, categories: action.categories, item: action.item, loading: false };

        default:
            return { ...state };
    }
}