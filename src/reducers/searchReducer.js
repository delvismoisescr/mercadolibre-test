import {
    SEARCH_ITEMS,
    UPDATE_SEARCH,
    RESET_SEARCH
} from '../actions/ActionTypes';

const INITIAL_STATE = {
    author: {},
    categories: [],
    items: [],
    loading: true,
    search: ''
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case RESET_SEARCH:
            return { ...state, author: {}, categories: [], items: [] };

        case UPDATE_SEARCH:
            return { ...state, search: action.search };

        case SEARCH_ITEMS:
            return { ...action.props, loading: action.loading, search: action.search };

        default:
            return { ...state };
    }
}