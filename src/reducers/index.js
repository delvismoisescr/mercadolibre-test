import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import searchReducer from './searchReducer';
import itemReducer from './itemReducer';

const rootReducer = combineReducers({
	searchReducer,
	formReducer,
	itemReducer
});

export default rootReducer;
