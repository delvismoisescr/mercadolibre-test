import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";
import { updateSearch, resetProps } from '../../actions/searchActions';
import './Header.sass';

class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {};
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.props.updateSearch(event.target.value);
    }

    handleSubmit(event) {
        if (this.props.search) {
            this.props.history.push(`/items?search=${this.props.search}`);
        } else {
            this.props.history.push(`/`);
        }
        event.preventDefault();
    }

    render() {
        return (
            <div className="header-mercadolibre">
                <div className="container-header">
                    <Link to="/" className="href-home">
                        <img src={require("../../assets/logo.png")} className="logo-header" alt="Logo"></img>
                    </Link>
                    <form onSubmit={this.handleSubmit} className="form-search">
                        <input
                            placeholder="Nunca dejes de buscar"
                            className="input-search"
                            value={this.props.search}
                            onChange={this.handleChange}>
                        </input>
                        <div className="button-search" onClick={this.handleSubmit}>
                            <img src={require("../../assets/search.png")} className="img-search" alt="Search"></img>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

function mapStateToProps({ searchReducer }) {
    return {
        ...searchReducer
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            updateSearch,
            resetProps
        },
        dispatch
    );
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));