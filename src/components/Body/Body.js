import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import Routes from "../../config/Routes";
import './Body.sass';

class Body extends Component {
	render() {
		return (
			<div
				className="container-body"
			>
				<Routes />
			</div>
		);
	}
}
function mapStateToProps(state) {
	return {

	};
}

export default withRouter(connect(mapStateToProps)(Body));
