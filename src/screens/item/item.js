import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { getItem, resetItem } from '../../actions/itemActions';
import './item.sass';

class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            filters: {
                name: 'Delvis Moisés',
                lastname: 'Cedeño Rodríguez'
            }
        };
        this.renderPicture = this.renderPicture.bind(this);
    }

    static getDerivedStateFromProps(props, state) {
        const id = props.match.params.id;
        if (id !== state.id) {
            props.resetItem();
            props.getItem(id, state.filters);
            return { id };
        }
        return null;
    }

    renderBreadCrumb() {
        if (this.props.categories && this.props.categories.length > 0) {
            return this.props.categories.map((value, index) => (
                <span key={index}>
                    {value} {index + 1 === this.props.categories.length ? '' : '> '}
                </span>
            ));
        }
        return;
    }

    renderPicture() {
        if (this.props.item && Object.keys(this.props.item).length > 0) {
            return (
                <img src={this.props.item.picture[0].url} alt={this.props.item.title} className="item-image-big" ></img>
            );
        }
        return;
    }

    renderInfo() {
        if (this.props.item && Object.keys(this.props.item).length > 0) {
            return (
                <div className="info-item">
                    <span className="new-sold">
                        {this.props.item.condition === 'new' ? 'Nuevo' : 'Usado'}
                        {` - ${this.props.item.sold_quantity} vendidos`}
                    </span>
                    <span className="title-item">{this.props.item.title}</span>
                    <span className="price-item">{this.props.item.price.currency === 'ARS' ? '$' : ''} {this.props.item.price.amount}</span>
                    <button className="buy-button">Comprar</button>
                </div>
            );
        }
        return;
    }

    renderDescription() {
        if (this.props.item && Object.keys(this.props.item).length > 0) {
            return (
                <div className="description-item">
                    <span className="span-description">Descripción del producto</span>
                    <p className="p-description">{this.props.item.description}</p>
                </div>
            );
        }
        return;
    }

    render() {
        return (
            <div className="container-item">
                <div className="breadcrumb" >
                    {this.renderBreadCrumb()}
                </div>
                <div className="header-item">
                    <div className="container-item-image">
                        {this.renderPicture()}
                    </div>
                    {this.renderInfo()}
                </div>
                <div className="container-description">
                    {this.renderDescription()}
                    <div className="description-spacer"></div>
                </div>
            </div>
        );
    }
}

function mapStateToProps({ itemReducer }) {
    return {
        ...itemReducer
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            getItem,
            resetItem
        },
        dispatch
    );
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Item));
