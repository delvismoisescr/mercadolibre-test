import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { searchItems, resetProps } from '../../actions/searchActions';
import { Link } from "react-router-dom";
import './search.sass';

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: '',
            filters: {
                limit: 4,
                name: 'Delvis Moisés',
                lastname: 'Cedeño Rodríguez'
            }
        };
        this.renderBreadCrumb = this.renderBreadCrumb.bind(this);
        this.renderItems = this.renderItems.bind(this);
    }

    static getDerivedStateFromProps(props, state) {
        const search = new URLSearchParams(props.location.search).get('search');
        if (search !== state.search) {
            if (search) {
                props.resetProps();
                props.searchItems(search, state.filters);
            } else {
                props.history.push(`/`);
            }
            return { search };
        }
        return null;
    }

    renderBreadCrumb() {
        if (this.props.categories && this.props.categories.length > 0) {
            return this.props.categories.map((value, index) => (
                <span key={index}>
                    {value} {index + 1 === this.props.categories.length ? '' : '> '}
                </span>
            ));
        }
        return;
    }

    renderItems() {
        if (this.props.items && this.props.items.length > 0) {
            return this.props.items.map((item) => (
                <div key={item.id} className="item">
                    <Link to={`/items/${item.id}`} className="href-item-image">
                        <img src={item.picture} className="item-image" alt={item.title}></img>
                    </Link>
                    <div className="item-info">
                        <div className="price-free-shipinng">
                            <span className="item-price">{item.price.currency === 'ARS' ? '$' : ''} {item.price.amount}</span>
                            {item.free_shipping ?
                                <img src={require('../../assets/free-shipping.png')} className="free-shipping-image" alt="free-shipping"></img>
                                : null
                            }
                        </div>
                        <Link to={`/items/${item.id}`} className="href-item-title">
                            <span className="item-title">{item.title}</span>
                        </Link>
                    </div>
                    <div className="address-container">
                        <span className="item-address">{item.address}</span>
                    </div>
                </div>
            ));
        }
        return;
    }

    render() {
        return (
            <div className="container-search">
                <div className="breadcrumb" >
                    {this.renderBreadCrumb()}
                </div>
                <div className="container-items">
                    {this.renderItems()}
                </div>
            </div>
        );
    }
}

function mapStateToProps({ searchReducer }) {
    return {
        ...searchReducer
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            searchItems,
            resetProps
        },
        dispatch
    );
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Search));
