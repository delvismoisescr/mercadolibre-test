import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import './home.sass';

class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		return (
			<div></div>
		);
	}
}

function mapStateToProps(store) {
	return {

	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators(
		{

		},
		dispatch
	);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Home));
