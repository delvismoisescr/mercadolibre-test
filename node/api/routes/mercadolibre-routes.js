// GENERADOR DE RUTAS EXPRESS
const Router = require('co-router');
const router = Router();

// CONTROLADORES
const itemsFunctions = require('../controllers/items-controller');

// RUTAS

//ITEMS
router.get('/items', itemsFunctions.getItems);
router.post('/items/:id', itemsFunctions.getItem);

module.exports = router;