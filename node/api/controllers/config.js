// FUNCIONES PARA CONFIGURAR LA INFORMACIÓN DE LAS RESPUESTAS

// FORMATEO DE CATEGORIAS
const mergeCategories = function mergeCategories(categories, filters) {
    try {
        filters.map((item) => {
            if (item.id === 'category') {
                return item.values.map((categorie) => {
                    return categorie.path_from_root.map((path) => {
                        categories.push(path.name);
                    })
                })
            }
        })
    } catch {
        categories = [];
    }
}

// FORMATEO DEL JSON DEL LISTADO DE ITEMS
const mergeItems = function mergeItems(items, results, limit) {
    limit = results.length < limit ? results.length : limit;
    for (let i = 0; i < limit; i++) {
        const format = {
            id: results[i].id,
            title: results[i].title,
            price: {
                currency: results[i].currency_id,
                amount: results[i].price,
                decimals: 0,
            },
            picture: results[i].thumbnail,
            condition: results[i].condition,
            free_shipping: results[i].shipping.free_shipping,
            address: results[i].address.state_name
        }
        items.push(format);
    }
}

// FORMATEO DEL JSON DE UN ITEM
const mergeItem = function mergeItem(data, item, description) {
    data.item = {
        id: item.id,
        title: item.title,
        price: {
            currency: item.currency_id,
            amount: item.price,
            decimals: 0,
        },
        picture: item.pictures,
        condition: item.condition,
        free_shipping: item.shipping.free_shipping,
        sold_quantity: item.sold_quantity,
        description: description.plain_text
    };
}

module.exports = Object.freeze({
    mergeCategories,
    mergeItems,
    mergeItem
});