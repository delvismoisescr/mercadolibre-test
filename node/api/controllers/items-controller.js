const fetch = require('node-fetch');
const Config = require('./config');

// CREE UN FORMATO JSON DE RESPUESTA ESTANDAR "{data, status, error}"

class ItemsController {

    static * getItems(req, res) {
        // RECIBO LOS PARAMETROS PARA LIMITAR LOS REGISTROS Y FIRMAR EL JSON
        // EN CASO DE NO RECIBIR PARAMETROS SETEA DATOS POR DEFECTO
        const search = req.query.q ? req.query.q : 'iphone';
        const limit = req.query.limit ? req.query.limit : 4;
        const name = req.query.name ? req.query.name : 'Delvis Moisés';
        const lastname = req.query.lastname ? req.query.lastname : 'Cedeño Rodriguez';

        // FORMATEO DE FIRMA JSON
        const data = { author: { name, lastname } };

        // PERICION PARA LA BUSQUEDA DE ITEMS
        fetch(`https://api.mercadolibre.com/sites/MLA/search?q=${search}`)
            .then(response => response.json())
            .then(json => {
                try {
                    data.categories = [];
                    data.items = [];

                    // FORMATEO DE CATEGORIAS
                    Config.mergeCategories(data.categories, json.filters);

                    // FORMATEO DE ITEMS
                    Config.mergeItems(data.items, json.results, limit);

                    return res.status(200).send({ data, status: 200, error: '' });
                } catch (error) {
                    return res.status(500).send({ data, status: 500, error: 'Error server, no se pudo obtener el listado de items.' });
                }
            })
            .catch(() => res.status(500).send({ data, status: 500, error: 'Error server, no se pudo obtener el listado de items.' }));
    }

    static * getItem(req, res) {
        // RECIBO EL ID DEL ITEM Y PARAMETROS PARA FIRMAR EL JSON
        // EN CASO DE NO RECIBIR PARAMETROS SETEA DATOS POR DEFECTO
        const id = req.params.id ? req.params.id : '';
        const name = req.body.name ? req.body.name : 'Delvis Moisés';
        const lastname = req.body.lastname ? req.body.lastname : 'Cedeño Rodriguez';

        // FORMATEO DE FIRMA JSON
        const data = { author: { name, lastname } };
        const promises = [];
        promises.push(
            fetch(`https://api.mercadolibre.com/items/${id}`)
                .then(response => response.json())
                .then(json => ({ data: json, status: 200, error: '' }))
                .catch(() => ({ data, status: 500, error: 'Error server, no se pudo obtener el item.' }))
        );
        promises.push(
            fetch(`https://api.mercadolibre.com/items/${id}/description`)
                .then(response => response.json())
                .then(json => ({ data: json, status: 200, error: '' }))
                .catch(() => ({ data, status: 500, error: 'Error server, no se pudo obtener la descripción del item.' }))
        );

        // EJECUTO LAS DOS PETICIONES EN MODO PROMESA PARA OBTENER LOS RESULTADOS AL MISMO TIEMPO
        Promise.all(promises)
            .then(([item, description]) => {
                try {
                    // DETERMINO EL ESTADO DE LAS PROMESAS
                    if (item.status === 200 && description.status === 200) {

                        // NOTA: AGREGO UNA PETICION ADICIONAL PARA PODER OBTENER LA CATEGORIA DE UN ITEM ESPECIFICO
                        // Y PODER ARMAR EL BREADCRUMD
                        fetch(`https://api.mercadolibre.com/sites/MLA/search?category=${item.data.category_id}`)
                            .then(response => response.json())
                            .then(category => {
                                data.categories = [];
                                Config.mergeCategories(data.categories, category.filters);
                                Config.mergeItem(data, item.data, description.data);
                                return res.status(200).send({ data, status: 200, error: '' });
                            })
                            .catch(() => ({ data, status: 500, error: 'Error server, no se pudo obtener la categoría del item.' }))
                    } else if (item.status !== 200) {
                        res.status(item.status).send({ data, status: item.status, error: item.error });
                    } else if (description.status !== 200) {
                        res.status(description.status).send({ data, status: description.status, error: description.error });
                    } else {
                        res.status(500).send({ data, status: 500, error: 'Error server, no se pudo consultar el item.' });
                    }
                } catch (e) {
                    res.status(500).send({ data, status: 500, error: 'Error server, no se pudo consultar el item.' });
                }
            });
    }

}

module.exports = ItemsController;