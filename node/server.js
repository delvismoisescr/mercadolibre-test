// RUTA BASE DEL SERVICIO
const basePath = '/api/';

// PARSER PARA CONVERTIR EL PAYLOAD EN JSON (PARA POST/PUT REQUEST)
const bodyParser = require('body-parser');

// MIDDLEWARE PARA EL ACCESO Y CONTROL DE LAS PETICIOES
var cors = require('cors')

// FRAMEWORK EXPRESS NODE UTILIZADO EN ESTE CASO PARA EL ROUTER DE LA API
var express = require('express'),
    app = express(),
    port = process.env.PORT || 3005; // PUERTO
    
app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(`${basePath}health`, require('express-healthcheck')());
app.use(`${basePath}`, require('./api/routes/mercadolibre-routes'));
app.listen(port);

console.log('Mercadolibre node ' + port);
